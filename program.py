import pandas as pd
import numpy as np
from sklearn.cluster import KMeans
from sklearn.preprocessing import LabelEncoder
from sklearn.preprocessing import MinMaxScaler
import seaborn as sns
import matplotlib.pyplot as plt
import math

def doKmeans(X, nclust=2):
    model = KMeans(nclust)
    model.fit(X)
    clust_labels = model.predict(X)
    cent = model.cluster_centers_
    return (clust_labels, cent)

train = pd.read_csv("googleplaystore.csv")

correctedInstalls = []
for item in train['Installs']:
    withoutPlus = item[:-1]
    withoutComma = withoutPlus.replace(',', '')
    convertedToInt = int(withoutComma) if withoutComma != '' else 0
    correctedInstalls.append(convertedToInt)

correctedPrice = []
for item in train['Price']:
    if item[0] == '$':
        withoutDollar = item[1:]
    else:
        withoutDollar = item
    correctedPrice.append(withoutDollar)

correctedRating = []
for item in train['Rating']:
    if math.isnan(item):
        withoutNaN = 0
    else:
        withoutNaN = item
    correctedRating.append(withoutNaN)

train['Installs'] = correctedInstalls
train['Price'] = correctedPrice
train['Rating'] = correctedRating

#print(train.isna().sum())

train = train.drop(['App','Category', 'Size','Type','Content Rating','Genres','Last Updated','Current Ver','Android Ver'], axis=1)

#train.info()

#print(train['Installs'])
#print(train.head())
#print(train.describe())

clust_labels, cent = doKmeans(train, 2)
kmeans = pd.DataFrame(clust_labels)
train.insert((train.shape[1]),'kmeans',kmeans)

#Plot the clusters obtained using k means
fig = plt.figure()
ax = fig.add_subplot(111)
scatter = ax.scatter(train['Rating'],train['Reviews'],c=kmeans[0],s=50)

ax.set_title('K-Means Clustering')
ax.set_xlabel('Rating')
ax.set_ylabel('Reviews')
plt.colorbar(scatter)
plt.show()